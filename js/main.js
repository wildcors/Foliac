var Regexp = {
    search : /^[а-яА-ЯёЁa-zA-Z0-9/\s/]+$/,
    name : /^[а-яА-ЯёЁa-zA-Z0-9-\ ]{1,20}$/,
    mail : /^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/,
    message : /^[а-яА-ЯёЁa-zA-Z0-9-_ )(\.,:;""]{1,300}$/
};
var loadImages = [];

function preloadImages () {
	for (var i = 0; i < preloadImages.arguments.length; i++) {
		loadImages[i] = new Image();
		loadImages[i].src = preloadImages.arguments[i];;
	}
}

preloadImages(
	"images/header-background-0.png",
	"images/header-background-1.png",
	"images/header-background-2.png"
	);

function autoSlider (slider, amountSlides) {
	var counter = 1;
	
	setInterval(function () {
		if (counter == amountSlides) {
			counter = 0;
		}
		slider.style.backgroundImage = "url(./images/header-background-" + counter + ".png";
		counter++;
	}, 5000);
};

autoSlider(document.querySelector(".auto-slider"), 3);

function contactForm (form) {
	var fields = {
		name : form.querySelector(".name"),
		mail : form.querySelector(".mail"),
		message : form.querySelector(".message")
	};
	var submit = document.querySelector(".submit"),
		v = 0;

	function checkField (field) {

		if (field.value != field.defaultValue) {
			if (Regexp[field.name].test(field.value)) {
				v++;
				field.style.border = "2px solid #58d99c";
			} else {
				field.style.border = "2px solid #f4776d";
				form.querySelector(".error-message").style.opacity = "1";
			}
		} else {
			field.style.border = "2px solid #f6ab25";
		}
	};

	function startCheck() {
		v = 0;
		for (var prop in fields) {
			checkField(fields[prop]);
		}
		if (v == 3) {
			form.querySelector(".error-message").style.opacity = "0";
			form.querySelector(".sending").style.opacity = "1";
		}
	};

	submit.addEventListener("click", startCheck);
}
contactForm (document.querySelector(".contact-form"));